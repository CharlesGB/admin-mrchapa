<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_controller extends CI_Controller
{

	public function __construct()
	{
		  parent::__construct();
	}

	public function template($view,$data=null)
	{

		/*echo "<pre>";
		print_r($data);*/


		$this->load->view("default/header");
		$this->load->view("default/aside");
		$this->load->view($view, $data);
		$this->load->view("default/footer");

	}

	public function template_login($view)
	{
		$this->load->view($view);

	}	

}


		
		





