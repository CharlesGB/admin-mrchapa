<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imprimir_fichas extends My_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->helper('form');

	}

	public function index(){

		$this->template("imprimir_fichas/index");

	}
}