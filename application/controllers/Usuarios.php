<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends My_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->helper('form');

	}

	//Carrega Template Listar Usuário
	public function index(){
		$this->template("usuarios/index");
	}

	//Carrega Template Listar Usuário
	public function listar_usuarios(){

		$this->load->model("Users_model");
		$data['result'] = $this->Users_model->get_user_data();

		//var_dump($data = $this->Users_model->get_user_data());
		$this->template("usuarios/listar_usuarios", $data);
	}

	//Carrega Template Adicionar Usuário
	public function adicionar_usuarios(){
		$this->template("usuarios/adicionar_usuarios");
	}

	// Editando Usuário
	public function edit_usuarios($id){
		
		//Chamando Model e metodo select()
		$this->load->model("Users_model");
		$data['result'] = $this->Users_model->select($id);
		
		//Template adicionar usuário
		$this->template("usuarios/editar_usuarios", $data);

	}

	//Deletenado Usuário
	public function delete_usuarios($id){

		//Chamando Model e metodo select()
		$this->load->model("Users_model");
		if ($data['result'] = $this->Users_model->delete($id)) {
			
			echo "Excluido usuário " . $id;
			redirect(base_url("usuarios/listar_usuarios"));

		}
		
	}

	public function insert_users(){

		//Recebendo dados do Post
		$inputFullName = $this->input->post('inputFullName');
		$inputCpf = $this->input->post('inputCpf');
		$inputUser = $this->input->post('inputUser');
		$inputPhone = $this->input->post('inputPhone');
		$inputEmail = $this->input->post('inputEmail');
		$inputPassword = $this->input->post('inputPassword');
		$inputPasswordCheck = $this->input->post('inputPasswordCheck');
		$inputPaper = $this->input->post('inputPaper');
		$userReference = 100; //Identifica o ID da empresa
		//die;

		//Form Validation Rules
		$this->form_validation->set_rules('inputFullName', 'Nome Completo', 'required');
		$this->form_validation->set_rules('inputCpf', 'CPF', 'required|min_length[11]|max_length[11]');
		$this->form_validation->set_rules('inputUser', 'Usuário', 'required');
		$this->form_validation->set_rules('inputPhone', 'Telefone', 'required');
		$this->form_validation->set_rules('inputEmail', 'E-mail', 'trim|required|valid_email');
		$this->form_validation->set_rules('inputPassword', 'Senha', 'required');
		$this->form_validation->set_rules('inputPasswordCheck', 'Repetir Senha', 'required|matches[inputPassword]');
		$this->form_validation->set_rules('inputPaper', 'Papel', 'required');

		if ($this->form_validation->run() == FALSE) {

			$erros = array('mensagens' => validation_errors());
			$this->template("usuarios/adicionar_usuarios", $erros);

		} else {
			//$data = array('mensagens' => 'Usuário cadastrado com sucesso!');
			//$this->template("usuarios/adicionar_usuarios", $data);

			//Criptografando Senha
			$inputPassHash = md5($inputPassword);

			//Criando Array com os values
			$data = array('user_id'=>NULL, 'user_name'=>$inputUser, 'user_fullname'=>$inputFullName, 'user_email'=>$inputEmail, 'user_telefone'=>$inputPhone, 'user_cpf'=>$inputCpf,  'user_password_hash'=>$inputPassHash, 'user_type'=>$inputPaper, 'user_reference'=>$userReference, 'user_date_create'=>NULL);

			//Chamando Model e metodo insert()
			$this->load->model("Users_model");
			$result = $this->Users_model->insert($data);

			//Redireciona pra página Adicionar Usuários
			redirect(base_url("usuarios/adicionar_usuarios"));
		}
		
	}

	public function update_user($id){

		//Recebendo dados do Post

		$id = $id;
		$inputFullName = $this->input->post('inputFullName');
		$inputCpf = $this->input->post('inputCpf');
		$inputUser = $this->input->post('inputUser');
		$inputPhone = $this->input->post('inputPhone');
		$inputEmail = $this->input->post('inputEmail');
		$inputPassword = $this->input->post('inputPassword');
		$inputPasswordCheck = $this->input->post('inputPasswordCheck');
		$inputPaper = $this->input->post('inputPaper');
		$userReference = 100; //Identifica o ID da empresa
		

		//Form Validation Rules
		$this->form_validation->set_rules('inputFullName', 'Nome Completo', 'required');
		$this->form_validation->set_rules('inputCpf', 'CPF', 'required|min_length[11]|max_length[11]');
		$this->form_validation->set_rules('inputUser', 'Usuário', 'required');
		$this->form_validation->set_rules('inputPhone', 'Telefone', 'required');
		$this->form_validation->set_rules('inputEmail', 'E-mail', 'trim|required|valid_email');
		$this->form_validation->set_rules('inputPassword', 'Senha', 'required');
		$this->form_validation->set_rules('inputPasswordCheck', 'Repetir Senha', 'required|matches[inputPassword]');
		$this->form_validation->set_rules('inputPaper', 'Papel', 'required');


		if ($this->form_validation->run() == FALSE) {

			$erros = array('mensagens' => validation_errors());
			$this->template("usuarios/editar_usuarios", $erros);

		} else {
			//$data = array('mensagens' => 'Usuário cadastrado com sucesso!');
			//$this->template("usuarios/adicionar_usuarios", $data);

			//Criptografando Senha
			$inputPassHash = md5($inputPassword);

			//Criando Array com os values
			$data = array('user_id'=>$id, 'user_name'=>$inputUser, 'user_fullname'=>$inputFullName, 'user_email'=>$inputEmail, 'user_telefone'=>$inputPhone, 'user_cpf'=>$inputCpf,  'user_password_hash'=>$inputPassHash, 'user_type'=>$inputPaper, 'user_reference'=>$userReference, 'user_date_create'=>NULL);

			//Chamando Model e metodo insert()
			$this->load->model("Users_model");
			$result = $this->Users_model->update($id,$data);
			
			/*echo "to aqui";
			echo "<br>";
			var_dump($data);
			die;*/			

			//Redireciona pra página Adicionar Usuários
			redirect(base_url("usuarios/listar_usuarios"));
		}
		
	}

}