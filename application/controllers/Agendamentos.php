<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agendamentos extends My_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->helper('form');

	}
		public function index(){

		$this->template("agendamentos/index");

	}
}