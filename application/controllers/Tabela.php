<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabela extends My_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->helper('form');

	}

	public function index(){

		$this->template("tabela/vendas");

	}

	public function listar_vendas(){
		$this->template("tabela/vendas");
	}

	public function listar_gastos(){
		$this->template("tabela/gastos");
	}
}