<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends My_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('form');


	}

	public function page($data=null)
	{
		if ($this->session->userdata("user_id")) {

			/*var_dump($data);
			die;*/

			if ($_SESSION['tipo'] == 1) {

				$this->template("restrict_admin", $data);
				//redirect(base_url()."restrict_admin");

			}else{

				$this->template("restrict_sales");
				//redirect(base_url()."restrict_sales");
			}

		}else{

			$this->template_login("login");
		}		
	}

	//Validação
	public function index()	
	{

		$username = $this->input->post('user');
		$password = $this->input->post('pass');
		//var_dump($username . $password );

		$this->form_validation->set_rules('user', 'Usuário', 'required|trim|min_length[5]|max_length[12]|alpha');
		$this->form_validation->set_rules('pass', 'Senha', 'required');
		//array('required' => ' campo %s é obrigatório' )

		if ($this->form_validation->run()){	

			$this->load->model("Auth_model");
			$result = $this->Auth_model->get_user_data($username, $password);

			if ($result) {					
					//valores recuperados do banco
					$user_id = $result->user_id;

		    		$_SESSION['tipo'] = $result->user_type;
		    		$_SESSION['name'] = $result->user_fullname;
            		
            		$this->session->set_userdata("user_id", $user_id);

            		$this->load->model("Dashboard_model");
            		$data['qtdVendas'] = $this->Dashboard_model->get_dashboard_sale(date('n'));
            		//$data2['qtdVendas2'] = $this->Dashboard_model->get_dashboard_saleRS(date('n'));

               		$this->page($data);

            	}else{ 
            		 
            		echo "Error";
            		//$this->load->view('login');
            		redirect(base_url());
            	} 		

        }else{

        	$this->page();
        }

	}

	public function logout(){

		//$this->session->unset_userdata("user_id");
		session_destroy();
		redirect(base_url());
	}

}


		
		





