
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Painel</h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
            <div class="panel-body">Panel Content</div>
            <div class="panel-footer">Panel Footer</div>
          </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
            <div class="panel-body">Panel Content</div>
            <div class="panel-footer">Panel Footer</div>
          </div>
        </div>    
      </div>

      <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
            <div class="panel-body">Panel Content</div>
            <div class="panel-footer">Panel Footer</div>
          </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
            <div class="panel-body">Panel Content</div>
            <div class="panel-footer">Panel Footer</div>
          </div>
        </div>    
      </div>
      <!-- Main row -->  
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->

  