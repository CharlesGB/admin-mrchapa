  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>EDITAR USUÁRIOS</h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">

          <?php foreach($result as $item){ ?>

          <!--<?php //echo form_open('usuarios/update_user/0'); ?>-->
          <?php echo form_open( base_url('usuarios/update_user/' . $item['user_id']) ); ?> 
            
            <div class="form-row">

              <div class="row">

                
                <div class="form-group col-md-6">
                  <label for="inputFullName">Nome Completo*</label>
                  <?php echo form_input(array('class' => 'form-control', 'name'=>'inputFullName', 'placeholder'=>'Nome completo', 'value'=> $item['user_fullname']));
                //echo form_error('usuario'); ?>
                </div>

                <div class="form-group col-md-6">
                  <label for="inputCpf">CPF (Somente números)*</label>
                  <?php echo form_input(array('class' => 'form-control', 'name'=>'inputCpf', 'placeholder'=>'CPF', 'value'=> $item['user_cpf'] ));
                  //echo form_error('usuario'); ?>
                </div>

                <div class="form-group col-md-6">
                  <label for="inputUser">Usuário*</label>
                  <?php echo form_input(array('class' => 'form-control', 'name'=>'inputUser', 'placeholder'=>'Usuário', 'value'=> $item['user_name'] ));
                  //echo form_error('usuario'); ?>
                </div>

                <div class="form-group col-md-6">
                  <label for="inputPhone">Telefone</label>
                  <?php echo form_input(array('class' => 'form-control', 'name'=>'inputPhone', 'placeholder'=>'Telefone', 'value'=> $item['user_telefone'] ));
                  //echo form_error('usuario'); ?>
                </div> 

              </div>

              <div class="row"> 

                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="inputEmail">E-mail*</label>
                    <?php echo form_input(array('class' => 'form-control', 'name'=>'inputEmail', 'placeholder'=>'E-mail', 'value'=> $item['user_email']));
                    //echo form_error('usuario'); ?>
                  </div>
               </div>

               <div class="form-group col-md-6">
                 <label for="inputPassword">Senha*</label>
                 <?php echo form_input(array('class' => 'form-control', 'type' => 'password', 'name'=>'inputPassword', 'placeholder'=>'Senha'));
                 //echo form_error('usuario'); ?>
               </div>

              </div>

              <div class="row">  
                <div class="form-group col-md-6">
                  <label for="inputPasswordCheck">Repitir Senha*</label>
                  <?php echo form_input(array('class' => 'form-control', 'type' => 'password', 'name'=>'inputPasswordCheck', 'placeholder'=>'Repetir Senha'));
                  //echo form_error('usuario'); ?>
                </div>
              </div>

              <div class="row">
                <div class="form-group col-md-6">
                  <label for="inputState">Papel de usuário*</label>
                    <select id="inputState" class="form-control" name="inputPaper">
                      <option selected value="1">Administrador</option>
                      <option value="2">Usuário</option>
                    </select>
                </div>

              </div>

            </div>
          </div>
        </div>

            <button type="submit" class="btn btn-primary">Atualizar</button> 

            <?php     
              }  
            ?>


       <?php 
         echo form_close();  
         echo "<div>&nbsp</div>";
         //echo validation_errors();
         echo validation_errors('<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close" style="text-decoration: none;">&times;</a>','</div>');
       ?>

        </div>    
      </div>
      <!-- Main row -->  
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
