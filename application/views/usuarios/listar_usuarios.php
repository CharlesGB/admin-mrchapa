
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>LISTAR USUÁRIOS</h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
    <!-- Main content -->
           <div class="box">
            <div class="box-header">
              <!--<h3 class="box-title">Data Table With Full Features</h3>-->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nome</th>
                  <th>E-mail</th>
                  <th>Telefone</th>
                  <th>CPF</th>
                  <th>Papel</th>
                  <th>Data de Cadastro</th>
                  <th>Ação</th>
                </tr>
                </thead>
                <tbody>
                <?php     
                  foreach($result as $item){  
                ?>

                <tr>    
                  <td><?php echo $item['user_fullname'] ?> </td>
                  <td><?php echo $item['user_email'] ?> </td>
                  <td><?php echo $item['user_telefone'] ?> </td>
                  <td><?php echo $item['user_cpf'] ?> </td>
                  <td>
                    <?php if ($item['user_type'] == 1) {
                          echo "Administrador";
                        }else{
                          echo "Usuário";
                      } ?> 
                  </td>
                  <td><?php echo date('d/m/Y H:i:s', strtotime($item['user_date_create'])) ?> </td>
                  <td>

                    <a type="button" href="<?= base_url('usuarios/edit_usuarios/' . $item['user_id']) ?>" class="btn btn-info btn-sm">Editar</a>

                    <a type="button" href="<?= base_url('usuarios/delete_usuarios/' . $item['user_id']) ?>" class="btn btn-danger btn-sm">Excluir</a>


                  </td>
                </tr>
                <?php }?>

                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->  
      </div>
      <!-- Main row -->  
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->

