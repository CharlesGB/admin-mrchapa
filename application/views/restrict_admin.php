
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        PAINEL
        <!--<small>Control panel</small>-->
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">

        <div class="col-lg-3">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>
                <?php echo $qtdVendas; ?></h3>
                <p>Vendas Registradas em 
                  <strong>
                    <?php 
                      setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                      date_default_timezone_set('America/Sao_Paulo');
                      echo strftime('%B de %Y', strtotime('today')); 
                    ?>
                </strong>
              </p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">Mais Informações <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php //echo $qtdVendas2; ?><sup style="font-size: 20px"> R$</sup></h3>
              <p>Vendidos registrados em
                <strong>
                  <?php 
                  setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                  date_default_timezone_set('America/Sao_Paulo');
                  echo strftime('%B de %Y', strtotime('today')); 
                  ?>
                </strong>
              </p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">Mais Informações <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>04</h3>
              <p>Usuários registrados</p>
              </br>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">Mais Informações <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>65</h3>

              <p>Lembretes/Anotações</p>
              </br>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">Mais Informações <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->

      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
            <div class="panel-body">
              
              <canvas id="myChart2" width="400" height="200"></canvas>
              <script>
              var ctx = document.getElementById('myChart2');
              var myChart2 = new Chart(ctx, {
                  type: 'polarArea',
                  data: {
                      /*labels: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],*/
                      datasets: [{
                          label: '# of Votes',
                          data: [12, 15, 8, 5, 10, 4],
                          backgroundColor: [
                              'rgba(255, 99, 132, 0.5)',
                              'rgba(54, 162, 235, 0.5)',
                              'rgba(255, 206, 86, 0.5)',
                              'rgba(75, 192, 192, 0.5)',
                              'rgba(153, 102, 255, 0.5)',
                              'rgba(255, 159, 64, 0.5)'
                          ],
                          borderColor: [
                              'rgba(255, 99, 132, 1)',
                              'rgba(54, 162, 235, 1)',
                              'rgba(255, 206, 86, 1)',
                              'rgba(75, 192, 192, 1)',
                              'rgba(153, 102, 255, 1)',
                              'rgba(255, 159, 64, 1)'
                          ],
                          borderWidth: 1
                      }]
                  },
                  /*options: {
                      scales: {
                          yAxes: [{
                              ticks: {
                                  beginAtZero: true
                              }
                          }]
                      }
                  }*/
              });
              </script>

            </div>
            <div class="panel-footer">Panel Footer</div>
          </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
            <div class="panel-body">
              
              <canvas id="myChart" width="400" height="200"></canvas>
              <script>
              var ctx = document.getElementById('myChart');
              var myChart = new Chart(ctx, {
                  type: 'pie',
                  data: {
                      labels: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                      datasets: [{
                          label: '# of Votes',
                          data: [12, 19, 3, 5, 2, 3],
                          backgroundColor: [
                              'rgba(255, 99, 132, 0.8)',
                              'rgba(54, 162, 235, 0.8)',
                              'rgba(255, 206, 86, 0.8)',
                              'rgba(75, 192, 192, 0.8)',
                              'rgba(153, 102, 255, 0.8)',
                              'rgba(255, 159, 64, 0.8)'
                          ],
                          borderColor: [
                              'rgba(255, 99, 132, 1)',
                              'rgba(54, 162, 235, 1)',
                              'rgba(255, 206, 86, 1)',
                              'rgba(75, 192, 192, 1)',
                              'rgba(153, 102, 255, 1)',
                              'rgba(255, 159, 64, 1)'
                          ],
                          borderWidth: 1
                      }]
                  },
                  /*options: {
                      scales: {
                          yAxes: [{
                              ticks: {
                                  beginAtZero: true
                              }
                          }]
                      }
                  }*/
              });
              </script>


            </div>
            <div class="panel-footer">Panel Footer</div>
          </div>
        </div>    
      </div>

      <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
            <div class="panel-body">Panel Content</div>
            <div class="panel-footer">Panel Footer</div>
          </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
            <div class="panel-body">Panel Content</div>
            <div class="panel-footer">Panel Footer</div>
          </div>
        </div>    
      </div>
      <!-- Main row -->  
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->


