<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SALE APP</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/restrict-schema/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/restrict-schema/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/restrict-schema/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/restrict-schema/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/restrict-schema/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/restrict-schema/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/restrict-schema/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/restrict-schema/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/restrict-schema/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/restrict-schema/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- Data Table-->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/restrict-schema/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/restrict-schema/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/restrict-schema/dist/css/skins/_all-skins.min.css">

  <!------------------- DATA TABLE ------------------------------------------->

  <!--<link rel="stylesheet" href="<?php //echo base_url(); ?>assets/restrict-schema/bower_components/bootstrap/dist/css/bootstrap.min.css">
  Font Awesome
  <link rel="stylesheet" href="<?php //echo base_url(); ?>assets/restrict-schema/bower_components/font-awesome/css/font-awesome.min.css">

  Ionicons
  <link rel="stylesheet" href="<?php //echo base_url(); ?>assets/restrict-schema/bower_components/Ionicons/css/ionicons.min.css">

  DataTables
  <link rel="stylesheet" href="<?php //echo base_url(); ?>assets/restrict-schema/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  Theme style 
  <link rel="stylesheet" href="<?php //echo base_url(); ?>assets/restrict-schema/dist/css/AdminLTE.min.css">

  AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. 
  <link rel="stylesheet" href="<?php //echo base_url(); ?>assets/restrict-schema/dist/css/skins/_all-skins.min.css">-->

  <!------------------- FIM DATA TABLE ------------------------------------------->

  <!-- Link Gráficos-->
  <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
  <!-- Link Tabela-->
  <link rel="stylesheet" href="<?php //echo base_url(); ?>assets/restrict-schema/dist/css/AdminLTE.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <!-- My Css -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/style.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">

  <!--<a class="btn btn-link" href="restrict/logoff"><i class="fa fa-sign-out"></i></a>-->
  <div class="wrapper">
    <header class="main-header">
      <!-- Logo -->
      <a href="<?php echo base_url(); ?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><i class="fa fa-arrow-right"></i></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><?php echo ucwords($_SESSION['name']); ?></span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a class="btn btn-link" href=<?= base_url('auth/logout')?>><i class="fa fa-sign-out"></i></a>
                <!--<img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                <span class="hidden-xs">Alexander Pierce</span>-->
              </a>
              
            </li>

          </ul>
        </div>      
      </nav>
    </header>
  