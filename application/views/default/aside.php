  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header" style="background-color: #3c8dbc;">&nbsp;</li>
        <!--<li class="header alert alert-danger">Restam 14 dias de TRIAL</li>-->

        <li class="li-menu">
          <a href=<?= base_url()?>>
            <i class="fa fa-dashboard"></i><span>Painel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i><span>Tabelas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href=<?= base_url('tabela/listar_vendas')?>><i class="fa fa-circle-o"></i>Listar Vendas</a></li>
            <li><a href=<?= base_url('tabela/listar_gastos')?>><i class="fa fa-circle-o"></i>Listar Gastos</a></li>
          </ul>
        </li>


        <li class="li-menu">
          <a href=<?= base_url('grafico')?>>
            <i class="fa fa-bar-chart"></i><span>Gráficos</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-user-circle-o"></i><span>Usuários</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href=<?= base_url('usuarios/listar_usuarios')?>><i class="fa fa-circle-o"></i>Listar Usuários</a></li>
            <li class=""><a href=<?= base_url('usuarios/adicionar_usuarios')?>><i class="fa fa-circle-o"></i>Adicionar Usuário</a></li>
          </ul>
        </li>

        <li class="li-menu">
          <a href=<?= base_url('nova_venda')?>>
            <i class="fa fa-plus-square"></i><span>Nova venda</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        </li>

        <li class="li-menu">
          <a href=<?= base_url('imprimir_fichas')?>>
            <i class="fa fa-print"></i><span>Imprimir fichas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        </li>

         <li class="li-menu">
          <a href=<?= base_url('anotacoes')?>>
            <i class="fa fa-sticky-note"></i><span>Anotações</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        </li>

        <li class="li-menu">
          <a href=<?= base_url('agendamentos')?>>
            <i class="fa fa-calendar-check-o"></i><span>Agendamentos</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        </li>       

      </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
