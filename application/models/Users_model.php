<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model
{

	public function __construct()
	{

		parent::__construct();
		$this->load->database();

	}

	public function get_user_data()
	{

		$this->db
		->SELECT("user_id, user_name, user_fullname, user_email, user_telefone, user_cpf, user_password_hash, user_type, user_reference, user_date_create")
		->FROM("sale_users");

		$result = $this->db->get();
		return $result->result_array();
		//return $result->row();
	}

	public function select($id){

		$this->db->select("*");
		$this->db->from("sale_users");
		$this->db->where("user_id", $id);
		
		$result = $this->db->get();
		return $result->result_array();
	}

	public function insert($data){
		$this->db->insert("sale_users", $data);
		return TRUE;
	}

	public function update($id, $data){
		$this->db->where("user_id", $id);
		$this->db->update("sale_users", $data);
	}

	public function delete($id, $select = NULL){
		$this->db->where("user_id", $id);
		$this->db->delete("sale_users");

		return TRUE;
	}

}

?>