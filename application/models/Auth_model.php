<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model
{

	public function __construct()
	{

		parent::__construct();
		$this->load->database();

	}

	public function get_user_data($user, $pass)
	{

		$username = $user;
		$password = md5($pass);

		$this->db
			->SELECT("user_id, user_name, user_fullname, user_password_hash, user_type, user_reference, user_date_create")
			->FROM("sale_users")
			->WHERE("user_name", $username);

		$result = $this->db->get();

		if ($result->num_rows() > 0) {

			if ($result->row('user_password_hash') == $password) {
				
				return $result->row();
			}

		}else{
			return false;
		}

	}
	//funções CRUD
	public function get_data($id, $select = NULL)
	{
		if (!empty($select)) {
			//Verifica se o parametro $select foi passado
			$this->db->select($select);
		}
		$this->db->from("mrc_users");
		$this->db->where("user_id", $id);
		return $this->db->get();
	}

	public function insert($data){
		$this->db->insert("mrc_users", $data);
	}

	public function update($id, $data){
		$this->db->where("user_id", $id);
		$this->db->update("mrc_users", $data);
	}

	public function delete($id, $select = NULL){
		$this->db->where("user_id", $id);
		$this->db->delete("mrc_users");
	}

}

?>