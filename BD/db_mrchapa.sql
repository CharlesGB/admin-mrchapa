-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 21-Abr-2019 às 04:35
-- Versão do servidor: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_mrchapa`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `mrc_users`
--

CREATE TABLE `mrc_users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(45) NOT NULL,
  `user_fullname` varchar(45) NOT NULL,
  `user_password_hash` varchar(255) NOT NULL,
  `user_type` int(11) NOT NULL,
  `user_reference` int(11) NOT NULL,
  `user_date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `mrc_users`
--

INSERT INTO `mrc_users` (`user_id`, `user_name`, `user_fullname`, `user_password_hash`, `user_type`, `user_reference`, `user_date_create`) VALUES
(1, 'charles', 'charles braga', '$10$gs6iRezGtcb6Wd8UxEANx.clkCBd0Sz7g3wElvwQib6NqjpC25Isq', 1, 1, '2019-04-20 14:34:47'),
(2, 'luzia', 'luzia aparecida', '$10$gs6iRezGtcb6Wd8UxEANx.clkCBd0Sz7g3wElvwQib6NqjpC25Isq', 2, 2, '2019-04-20 14:41:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mrc_users`
--
ALTER TABLE `mrc_users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mrc_users`
--
ALTER TABLE `mrc_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
